<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nomor 4</title>
</head>
<body>
    <h1>Looping Pertama</h1>
    <?php
      for($no = 2; $no <= 20; $no+=2)
      {
          echo $no . "- I Love PHP <br/>" ;
      }
    ?>

    <h1>Looping Kedua</h1>
    <?php
      for($i = 20; $i > 0; $i-=2)
      {
          echo $i . "- I Love PHP <br/>" ;
      }
    ?>
</body>
</html>
